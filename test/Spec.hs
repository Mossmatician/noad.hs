import Music

input = unlines $
  [ "#title: Exercise 25"
  , "#bpb: 3"
  , "#bpm: 80"
  , "#tickVoice: 60"
  , "#tick: 2"
  , ""
  , "g3 x12 | c3 e3 c3"
  , "b2 e3 b2"
  , "a2 e3 a2"
  , "g2 e3 g2"
  , "f2 x3 | r a3 x2"
  , "d3 x3 | r a3 x2"
  , "g2 x6 | b3 x3 | d3 x2 e3"
  , "b3 x3 | f3 x2 g3"
  , ""
  , "g3 x12 | c3 e3 e3"
  , "b2 e3 e3"
  , "a2 e3 e3"
  , "g2 e3 e3"
  , "f2 x3 | a3 x4"
  , "a2 x3 | r g3 f3"
  , "c3 x3 | e3 /2 g3 /2 e3 /2 g3 /2 e3 /2 g3 /2"
  , "c3 x3 | e3 x3" ]

main :: IO ()
main = do
  case parsePiece input of
    Left e -> ioError $ userError $ "Parse error: " ++ show e
    Right mid -> putStr mid
  return ()
