# Noad.hs

Translates a textual representation of music into a format which can be compiled to midi by [midicomp](https://github.com/markc/midicomp). Created to play the teacher's parts in the exercises from [Solo Guitar Playing by Frederick M. Noad](https://archive.org/details/SoloGuitarPlayingFrederickM.Noad/).

## To-dos
+ Easier notation for chords
+ Starting a piece with a partial bar