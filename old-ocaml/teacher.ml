type noteInterval = NoteInterval of int * int * int * int;;
type noteEvent = Off of int * int | On of int * int * int;;

let intervalToEvents (ni : noteInterval) : noteEvent list =
  match ni with
    NoteInterval (t , d , p , v) -> [ On (t , p , v) ; Off (t + d , p) ];;

let eventToMidiEvent (ne : noteEvent) : string =
  match ne with
    On (t , p , v) -> (string_of_int t) ^ " On ch=1 n=" ^ (string_of_int p) ^ " v=" ^ (string_of_int v)
  | Off (t , p) -> (string_of_int t) ^ " Off ch=1 n=" ^ (string_of_int p) ^ " v=0"
;;

let eventTime (ne : noteEvent) : int = match ne with
    Off (t , _) -> t
  | On (t , _ , _) -> t
;;

let header (title : string) = "MFile 1 1 100
MTrk
0 Meta SeqName" ^ " \"" ^ title ^ "\"" ^ "
0 PrCh ch=1 p=25
0 PrCh ch=2 p=25
0 Tempo 500000
";;
let footer = "TrkEnd\n";;

type pSignature = { bpb : int; tpb : int; };;
type pAccidental = Flat | Natural | Sharp;;
type pNoteLetter = C | D | E | F | G | A | B;;
type pNoteName = PNoteName of pNoteLetter * pAccidental * int;;
type pSegment = PRest of int | PNote of pNoteName * int;;

let midiPitch (PNoteName (letter , acc , oct) : pNoteName) :int =
  let fromAcc = match acc with
      Flat -> -1
    | Natural -> 0
    | Sharp -> 1
  in
  let fromLetter = match letter with
      C -> 0
     | D -> 2 
     | E -> 4 
     | F -> 5 
     | G -> 7 
     | A -> 9 
     | B -> 11
  in fromLetter + fromAcc + (12 * oct) + 12;;

let rec seqBar (psig : pSignature) (start : int) (segs : pSegment list) : noteInterval list =
  match segs with
    [] -> []
  | (seg :: segs) ->
     match seg with
       PRest t -> seqBar psig (start + t * psig.tpb) segs
     | PNote (name , t) -> NoteInterval (start , (t * psig.tpb) , (midiPitch name) , 100) :: seqBar psig (start + t * psig.tpb) segs;;

let parBar (psig : pSignature) (start : int) (segss : pSegment list list) : noteInterval list =
  List.concat (List.map (seqBar psig start) segss);;

let rec seqParBars (psig : pSignature) (start : int) (barss : pSegment list list list) : noteInterval list =
  match barss with
    [] -> []
  | bar :: bars -> (parBar psig start bar) @ seqParBars psig (start + psig.bpb * psig.tpb) bars;;

let twobarsraw : pSegment list list list =
  [
    [
      [
        PNote (PNoteName (E , Natural , 2) , 4)
      ]
    ; [
        PNote (PNoteName (B , Natural , 3) , 1)
      ; PNote (PNoteName (A , Natural , 3) , 1)
      ; PNote (PNoteName (G , Natural , 3) , 1)
      ; PNote (PNoteName (F , Sharp , 3) , 1)
      ]
    ] ; [
      [
        PNote (PNoteName (E , Natural , 3) , 1)
      ; PNote (PNoteName (F , Sharp , 3) , 1)
      ; PNote (PNoteName (G , Natural , 3) , 1)
      ; PNote (PNoteName (E , Natural , 3) , 1)
      ]
    ]
  ];;

let twobars = seqParBars { bpb = 4 ; tpb = 200 } 0 twobarsraw;;

type musicFile =
  { title : string;
    psig  : pSignature;
    bars  : pSegment list list list;
  }

let musicFile2midi (mf : musicFile) : string =
  let tickLength = mf.psig.bpb * mf.psig.tpb in
  let rec tickRange t  = if t < tickLength
                        then t :: (tickRange (t + mf.psig.tpb))
                        else []
  in
  let tickIn = List.map (fun t -> NoteInterval (t , mf.psig.tpb , 60 , 50)) (tickRange 0)
  in
  let events = (List.sort (fun e1 e2 -> (eventTime e1) - (eventTime e2))
                     (List.concat
                        (List.map intervalToEvents
                           (tickIn @ (seqParBars mf.psig tickLength mf.bars)))))
  in
  let body = String.concat "\n"
               (List.map eventToMidiEvent events) ^ "\n"
  in
  let trkend = string_of_int (eventTime (List.hd (List.rev events))) ^ " Meta TrkEnd\n"
  in
  (header mf.title) ^ body ^ trkend ^ footer
;;

let ex5File : musicFile = { title = "Exercise 5" ;
                             psig = { bpb = 4 ; tpb = 200 } ;
                             bars = twobarsraw ; }
;;

let test_header = "#title: my test\n#bpb: 4\n#tpb: 200\n";;
let test_body = "c4 d4 e#4 x2\n";;
