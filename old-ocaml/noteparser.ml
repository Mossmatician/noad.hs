type token =
  | EOL
  | EOF
  | META of (string*string)
  | DIGITS of (int)
  | REST
  | NOTE_WITH_ACCIDENTAL of (Teacher.pNoteLetter * Teacher.pAccidental)
  | PARALLELBAR
  | DURATION

open Parsing;;
let _ = parse_error;;
# 2 "noteparser.mly"
    open Teacher;;

    let update_meta (key , value) ({ title ; psig = { bpb ; tpb } ; bars } as mfile) =
      match key with "title" -> { title = value ; psig = { bpb ; tpb } ; bars = bars }
                   | "bpb" -> { title = title ; psig = { bpb = (int_of_string value) ; tpb = tpb } ; bars = bars }
                   | "tpb" -> { title = title ; psig = { bpb = bpb ; tpb = (int_of_string value) } ; bars = bars }
                   | _ -> mfile

    let append_pbar { title ; psig ; bars } pbar =
      { title = title ; psig = psig ; bars = bars @ [ pbar ] }
# 25 "noteparser.ml"
let yytransl_const = [|
  257 (* EOL *);
    0 (* EOF *);
  260 (* REST *);
  262 (* PARALLELBAR *);
  263 (* DURATION *);
    0|]

let yytransl_block = [|
  258 (* META *);
  259 (* DIGITS *);
  261 (* NOTE_WITH_ACCIDENTAL *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\003\000\003\000\003\000\003\000\004\000\
\004\000\005\000\005\000\007\000\007\000\006\000\006\000\006\000\
\006\000\000\000"

let yylen = "\002\000\
\002\000\000\000\002\000\001\000\001\000\002\000\002\000\003\000\
\001\000\002\000\001\000\002\000\001\000\003\000\001\000\003\000\
\001\000\002\000"

let yydefred = "\000\000\
\002\000\000\000\018\000\000\000\004\000\001\000\005\000\000\000\
\000\000\003\000\000\000\000\000\011\000\000\000\000\000\012\000\
\006\000\007\000\000\000\010\000\000\000\016\000\000\000\014\000"

let yydgoto = "\002\000\
\003\000\004\000\010\000\011\000\012\000\013\000\014\000"

let yysindex = "\003\000\
\000\000\000\000\000\000\023\000\000\000\000\000\000\000\255\254\
\002\255\000\000\010\000\254\254\000\000\000\255\006\255\000\000\
\000\000\000\000\254\254\000\000\009\255\000\000\254\254\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\008\000\
\001\000\000\000\000\000\016\000\000\000\014\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\020\000\000\000"

let yygindex = "\000\000\
\000\000\000\000\000\000\000\000\250\255\244\255\000\000"

let yytablesize = 284
let yytable = "\020\000\
\013\000\008\000\009\000\001\000\016\000\015\000\021\000\017\000\
\022\000\018\000\020\000\024\000\023\000\015\000\000\000\009\000\
\000\000\000\000\000\000\008\000\000\000\000\000\006\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\013\000\000\000\000\000\013\000\013\000\013\000\013\000\
\017\000\000\000\017\000\017\000\017\000\017\000\015\000\019\000\
\009\000\015\000\015\000\015\000\008\000\009\000\000\000\005\000\
\007\000\008\000\008\000\009\000"

let yycheck = "\012\000\
\000\000\004\001\005\001\001\000\003\001\007\001\007\001\000\000\
\003\001\000\000\023\000\003\001\019\000\000\000\255\255\000\000\
\255\255\255\255\255\255\000\000\255\255\255\255\000\000\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\001\001\255\255\255\255\004\001\005\001\006\001\007\001\
\001\001\255\255\001\001\004\001\005\001\006\001\001\001\006\001\
\001\001\004\001\005\001\006\001\001\001\006\001\255\255\001\001\
\002\001\006\001\004\001\005\001"

let yynames_const = "\
  EOL\000\
  EOF\000\
  REST\000\
  PARALLELBAR\000\
  DURATION\000\
  "

let yynames_block = "\
  META\000\
  DIGITS\000\
  NOTE_WITH_ACCIDENTAL\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'input) in
    Obj.repr(
# 32 "noteparser.mly"
                ( _1 )
# 169 "noteparser.ml"
               : Teacher.musicFile))
; (fun __caml_parser_env ->
    Obj.repr(
# 35 "noteparser.mly"
                ( {title = "" ; psig = { bpb = 0 ; tpb = 0 } ; bars = [] } )
# 175 "noteparser.ml"
               : 'input))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'input) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'chunk) in
    Obj.repr(
# 36 "noteparser.mly"
                ( _2 _1 )
# 183 "noteparser.ml"
               : 'input))
; (fun __caml_parser_env ->
    Obj.repr(
# 39 "noteparser.mly"
        ( fun x -> x )
# 189 "noteparser.ml"
               : 'chunk))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string*string) in
    Obj.repr(
# 40 "noteparser.mly"
         ( fun x -> update_meta _1 x )
# 196 "noteparser.ml"
               : 'chunk))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'parBar) in
    Obj.repr(
# 41 "noteparser.mly"
               ( fun x -> append_pbar x _1 )
# 203 "noteparser.ml"
               : 'chunk))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'parBar) in
    Obj.repr(
# 42 "noteparser.mly"
               ( fun x -> append_pbar x _1 )
# 210 "noteparser.ml"
               : 'chunk))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'parBar) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'seqBar) in
    Obj.repr(
# 45 "noteparser.mly"
                              ( _1 @ [_3] )
# 218 "noteparser.ml"
               : 'parBar))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'seqBar) in
    Obj.repr(
# 46 "noteparser.mly"
           ( [_1] )
# 225 "noteparser.ml"
               : 'parBar))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'seqBar) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'atomicEvent) in
    Obj.repr(
# 49 "noteparser.mly"
                       ( _1 @ [_2] )
# 233 "noteparser.ml"
               : 'seqBar))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'atomicEvent) in
    Obj.repr(
# 50 "noteparser.mly"
                ( [_1] )
# 240 "noteparser.ml"
               : 'seqBar))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Teacher.pNoteLetter * Teacher.pAccidental) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 52 "noteparser.mly"
                                      ( let (c , a) = _1 in PNoteName (c , a , _2) )
# 248 "noteparser.ml"
               : 'notename))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Teacher.pNoteLetter * Teacher.pAccidental) in
    Obj.repr(
# 53 "noteparser.mly"
                         ( let (c , a) = _1 in PNoteName (c , a , 1) )
# 255 "noteparser.ml"
               : 'notename))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'notename) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 55 "noteparser.mly"
                                      ( PNote (_1 , _3) )
# 263 "noteparser.ml"
               : 'atomicEvent))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'notename) in
    Obj.repr(
# 56 "noteparser.mly"
             ( PNote (_1 , 1) )
# 270 "noteparser.ml"
               : 'atomicEvent))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 57 "noteparser.mly"
                         ( PRest _3 )
# 277 "noteparser.ml"
               : 'atomicEvent))
; (fun __caml_parser_env ->
    Obj.repr(
# 58 "noteparser.mly"
         ( PRest 1 )
# 283 "noteparser.ml"
               : 'atomicEvent))
(* Entry main *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let main (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Teacher.musicFile)
