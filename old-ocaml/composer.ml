open Teacher

let main () =
  (Lexing.from_channel stdin)
  |> Noteparser.main Notelexer.token
  |> musicFile2midi
  |> print_string
  ;;

main ()
