type token =
  | EOL
  | EOF
  | META of (string*string)
  | DIGITS of (int)
  | REST
  | NOTE_WITH_ACCIDENTAL of (Teacher.pNoteLetter * Teacher.pAccidental)
  | PARALLELBAR
  | DURATION

val main :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Teacher.musicFile
