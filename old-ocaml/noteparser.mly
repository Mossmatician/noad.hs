%{
    open Teacher;;

    let update_meta (key , value) ({ title ; psig = { bpb ; tpb } ; bars } as mfile) =
      match key with "title" -> { title = value ; psig = { bpb ; tpb } ; bars = bars }
                   | "bpb" -> { title = title ; psig = { bpb = (int_of_string value) ; tpb = tpb } ; bars = bars }
                   | "tpb" -> { title = title ; psig = { bpb = bpb ; tpb = (int_of_string value) } ; bars = bars }
                   | _ -> mfile

    let append_pbar { title ; psig ; bars } pbar =
      { title = title ; psig = psig ; bars = bars @ [ pbar ] }
%}

%token EOL EOF

%token <string*string> META

%token <int> DIGITS
%token REST

%token <Teacher.pNoteLetter * Teacher.pAccidental> NOTE_WITH_ACCIDENTAL
%token PARALLELBAR

%token DURATION


%start main
%type <Teacher.musicFile> main

%%

main: input EOF { $1 }

input:
  | /* empty */ { {title = "" ; psig = { bpb = 0 ; tpb = 0 } ; bars = [] } }
  | input chunk { $2 $1 }

chunk:
  | EOL { fun x -> x }
  | META { fun x -> update_meta $1 x }
  | parBar EOL { fun x -> append_pbar x $1 }
  | parBar EOF { fun x -> append_pbar x $1 }

parBar:
  | parBar PARALLELBAR seqBar { $1 @ [$3] }
  | seqBar { [$1] }

seqBar:
  | seqBar atomicEvent { $1 @ [$2] }
  | atomicEvent { [$1] }

notename: NOTE_WITH_ACCIDENTAL DIGITS { let (c , a) = $1 in PNoteName (c , a , $2) }
  | NOTE_WITH_ACCIDENTAL { let (c , a) = $1 in PNoteName (c , a , 1) }

atomicEvent: notename DURATION DIGITS { PNote ($1 , $3) }
  | notename { PNote ($1 , 1) }
  | REST DURATION DIGITS { PRest $3 }
  | REST { PRest 1 }

/* sequentialBar: */
/*   | atomicEvent { [ $1 ] } */
/*   | sequentialBar atomicEvent { $1 @ [ $2 ] } */

/* parallelBar: { } */
/*   | sequentialBar { [ $1 ] } */
/*   | parallelBar PARALLELBAR sequentialBar { $1 :: $3 } */
