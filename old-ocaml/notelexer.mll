{
  open Noteparser
  open Teacher

  let note_from_char (c : char) : pNoteLetter =
    match c with 'c' -> C
	       | 'C' -> C
	       | 'd' -> D
	       | 'D' -> D
	       | 'e' -> E
	       | 'E' -> E
	       | 'f' -> F
	       | 'F' -> F
	       | 'g' -> G
	       | 'G' -> G
	       | 'a' -> A
	       | 'A' -> A
	       | 'b' -> B
	       | 'B' -> B
	       | _ -> C
  ;;

  let acc_from_char (c : char) : pAccidental =
    match c with 'b' -> Flat
	       | '#' -> Sharp
	       | _ -> Natural

  exception UnknownCharacter of string
}

let ws = [' ' '\t']+
let nws = [^' ' '\t' '\n']
let toEOL = [^'\n']*

let noteChar = ['a'-'g''A'-'G']
let accChar = ['#' 'b']
let digits = ['0'-'9']+

rule token = parse
  | '#' (['A'-'Z' 'a'-'z']+ as key) ':' ws { meta_to_EOL key (Buffer.create 16) lexbuf }
  | '|' { PARALLELBAR }
  | 'r' { REST }
  | ['0'-'9']+ as value { DIGITS (int_of_string value) }
  | (noteChar as letter) (accChar as acc) { NOTE_WITH_ACCIDENTAL (note_from_char letter , acc_from_char acc) }
  | noteChar as letter { NOTE_WITH_ACCIDENTAL (note_from_char letter , Natural) }
  | 'x' { DURATION }
  | '\n' [' ' '\t' '\n']* eof? { EOL }
  | ws { token lexbuf }
  | eof { EOF }
  | _ { raise (UnknownCharacter (Lexing.lexeme lexbuf)) }

and meta_to_EOL key buf = parse
  | '\n' | eof { META (key , (Buffer.contents buf)) }
  | ws* nws+ as stuff { (Buffer.add_string buf stuff) ; meta_to_EOL key buf lexbuf }