module Blocks where

import Data.Ratio
import Control.Lens

data TimeInterval = Beats (Ratio Int)
type Note = Int
type Intensity = Ratio Int -- between 0 and 1

data MBlock = Pass
            | Rest TimeInterval
            | Env EnvironUp
            | Tone Note TimeInterval Intensity
            | Par [MBlock]
            | Seq MBlock MBlock

type EnvironUp = Environ -> Environ

data Environ = Environ 
