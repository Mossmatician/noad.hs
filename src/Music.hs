module Music where

import Data.Char
import Text.Parsec
import Data.Ratio
import Data.List (sortBy)

data Header = Header { title :: String , bpb :: Int , bpm :: Int , voice :: Int , percussion :: Int , partbar :: Int } deriving Show
type MFile = (Header , [[[Event]]])

type Pitch = Int
type TimeLen = Ratio Int
type Velocity = Int
type Channel = Int
data Event = Rest TimeLen | Tone Channel Pitch TimeLen Velocity deriving Show
type Modifier = Event -> Event

data MidiEvent = MidiEvent Int String deriving Show

-- Magic numbers
_midiTicks :: Ratio Int
_midiTicks = 96 -- time divisions per beat in midi file
_blankFile :: MFile
_blankFile = (Header
             "No title"
             4 -- beats per bar
             80 -- beats per minute
             24 -- should be nylon string acoustic guitar (officially
                -- that should be 25, but there's something funny in
                -- my setup).
             42 -- should be closed hi hat
             0 -- no partial bar at the start of the piece
            , [])
_velocity :: Int -- default velocity of notes
_velocity = 100
_duration :: TimeLen -- default duration of notes/rests
_duration = 1
_ch :: Channel -- most things are on this channel
_ch = 1
_tickDecay :: Ratio Int
_tickDecay = 5 % 6

-- General parser combinators
nbspace :: Parsec String st String
nbspace = many (oneOf " \t")

-- lexeme eats trailing non-breaking space, lineme also eats line breaks.
lexeme :: Parsec String st a -> Parsec String st a
lexeme p = do
  x <- p
  nbspace
  return x

lineme :: Parsec String st a -> Parsec String st a
lineme p = do
  x <- p
  spaces
  return x

chars2midiPitch :: Char -> Char -> Char -> Pitch
chars2midiPitch nl ac on = nlp + acp + onp * 12 + 12
  where nlp = case (toUpper nl) of
                'C' -> 0
                'D' -> 2
                'E' -> 4
                'F' -> 5
                'G' -> 7
                'A' -> 9
                'B' -> 11
        acp = case ac of
                '#' -> 1
                'n' -> 0
                'b' -> -1
        onp = ord on - 48

noteExp :: Parsec String st Event
noteExp = lexeme $ do
  nl <- oneOf "abcdefgABCDEFG"
  ac <- option 'n' (oneOf "#nb")
  on <- oneOf "23456"
  return (Tone _ch (chars2midiPitch nl ac on) _duration _velocity)

duration :: Parsec String st Modifier
duration = (try whole) <|> frac
  where
    whole = do
      char 'x'
      spaces
      nn <- many1 digit
      notFollowedBy (char '/' <|> digit)
      return (chDur $ read nn % 1)
    frac = do
      optional (do char 'x' ; spaces)
      nn <- option "1" (many1 digit)
      char '/'
      dd <- many1 digit
      return (chDur $ (read nn) % (read dd))
    chDur :: TimeLen -> Modifier
    chDur x (Rest _) = Rest x
    chDur x (Tone ch p _ v) = Tone ch p x v

velocity :: Parsec String st Modifier
velocity = do
  char 'v'
  spaces
  nn <- many1 digit
  return (chVel $ read nn)
  where
    chVel :: Int -> Modifier
    chVel v (Tone ch p x _) = Tone ch p x v
    chVel v e = e

modifier :: Parsec String st Modifier
modifier = lexeme $ do
  velocity <|> duration

rest :: Parsec String st Event
rest = lexeme $ do
  char 'r'
  return (Rest 1)

event :: Parsec String st Event
event = lexeme $ do
  e <- noteExp <|> rest
  m <- many (try modifier)
  return $ foldl (flip ($)) e m

bar :: MFile -> Parsec String st MFile
bar (h , evsss) = lineme $ do
  evss <- sepEndBy1 (many1 event) (lexeme $ char '|')
  return (h , evsss ++ [evss])

meta :: MFile -> Parsec String st MFile
meta (head@(Header _ bpb _ _ percussion _) , evsss) = lineme $ do
  char '#' ; spaces
  key <- many1 alphaNum
  spaces ; char ':' ; spaces
  value <- many (noneOf "\n")
  case key of
    "title" -> return (head { title = value } , evsss)
    "bpb"   -> return (head { bpb = (read value) } , evsss)
    "bpm"   -> return (head { bpm = (read value) } , evsss)
    "voice" -> return (head { voice = (read value) } , evsss)
    "tickVoice" -> return (head { percussion = (read value) } , evsss)
    "tick"  -> return (head , evsss ++ (take (read value) $ cycle [[tickBars]]))
    _       -> return (head , evsss)
    "partbar" -> return (head { partbar = (read value) } , evsss ++ [[take (bpb - partbar) $ tickBars]])
    where tickBars = let ch = 10
                         nn = percussion
                     in [ Tone ch nn 1 (round $ (fromIntegral _velocity) * _tickDecay^j)
                        | j <- [0 .. bpb - 1] ]
                         
fileLine :: MFile -> Parsec String st MFile
fileLine mf = (do lookAhead (char '#') ; meta mf) <|> bar mf

piece :: MFile -> Parsec String st MFile
piece mf = do
  (do eof ; return mf) <|>
    (do mf2 <- fileLine mf ; piece mf2)

renderHeader :: Header -> String
renderHeader (Header title bpb bpm voice ticks _) = unlines
  [ "MFile 1 1 " ++ (show $ round _midiTicks)
  , "MTrk"
  , "0 Meta SeqName \"" ++ title ++ "\""
  , "0 PrCh ch=1 p=" ++ (show voice)
  , "0 Tempo " ++ (show msPerBeat) ]
  where
    msPerBeat = 60 * 1000 * 1000 `quot` bpm

prerenderBar :: Int -> [[Event]] -> [MidiEvent]
prerenderBar t evss = concat $ map (prebar t) evss
  where
    prebar :: Int -> [Event] -> [MidiEvent]
    prebar _ [] = []
    prebar t (Rest l : es) = prebar (t + (round $ _midiTicks * l)) es
    prebar t (Tone ch p l v : es) =
      (MidiEvent t ("On ch=" ++ (show ch) ++ " n=" ++ (show p) ++ " v=" ++ (show v))) :
      (MidiEvent (t + (round $ _midiTicks * l)) ("Off ch=" ++ (show ch) ++ " n=" ++ (show p) ++ " v=0")) :
      prebar (t + (round $ _midiTicks * l)) es

renderMidiEvent :: MidiEvent -> String
renderMidiEvent (MidiEvent t s) = (show t) ++ " " ++ s ++ "\n"

renderPiece :: (Header,[[[Event]]]) -> String
renderPiece (h@(Header title bpb bpm voice ticks _) , esss) =
  (++) (renderHeader h) (concat $ map renderMidiEvent $
                          sortBy (\ (MidiEvent t1 _) (MidiEvent t2 _) -> t1 `compare` t2) $
                          concat $ map (\ (t , ess) -> prerenderBar t ess) $
                          zip [0,(round $ _midiTicks * (fromIntegral bpb))..] esss)
  ++ (show $ round $ _midiTicks * (fromIntegral $ bpb * length esss)) -- this is a lazy hack which will break in edge cases...
  ++ " Meta TrkEnd\nTrkEnd\n"

parsePiece :: String -> Either String String
parsePiece content = do
  case parse (piece _blankFile) "(stdin)" content of
    Left e -> Left $ "Parse error: \n" ++ (show e)
    Right ast -> Right $ renderPiece ast
