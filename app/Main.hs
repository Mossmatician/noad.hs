module Main where

import Music

main :: IO ()
main = do
  c <- getContents
  case parsePiece c of
    Left e -> ioError $ userError $ "Parse error: " ++ show e
    Right mid -> putStr mid
  return ()
